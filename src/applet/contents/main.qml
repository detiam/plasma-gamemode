// SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
// SPDX-FileCopyrightText: 2022 Harald Sitter <sitter@kde.org>

import QtQuick 2.15
import QtQuick.Controls 2.5 as QQC2
import QtQuick.Layouts 1.15

import org.kde.plasma.core 2.1 as PlasmaCore
import org.kde.plasma.extras 2.0 as PlasmaExtras
import org.kde.plasma.plasmoid 2.0

import org.kde.plasma.private.gamemode 0.1

Item {
    id: main

    Layout.preferredHeight: units.gridUnit * 10
    Layout.preferredWidth: units.gridUnit * 10
    Plasmoid.switchWidth: units.gridUnit * 5
    Plasmoid.switchHeight: units.gridUnit * 5

    Plasmoid.status: gamesEmpty() ? PlasmaCore.Types.PassiveStatus : PlasmaCore.Types.ActiveStatus
    // FIXME: iconing needs doing
    Plasmoid.icon: gamesEmpty() ? "input-gamepad-symbolic" : "applications-games"
    Plasmoid.toolTipMainText: gamesEmpty() ? i18nc("@info", "Gamemode Inactive") : i18nc("@info", "Gamemode Active")
    Plasmoid.toolTipSubText: i18ncp("@info process count in gamemode", "%1 Process", "%1 Processes", view.count)
    Plasmoid.compactRepresentation: PlasmaCore.IconItem {
        source: plasmoid.icon
        colorGroup: PlasmaCore.ColorScope.colorGroup
    }

    function gamesEmpty() {
        return gamesModel.rowCount <= 0
    }

    ListView {
        id: view
        anchors.fill: parent
        model: GamesModel {
            id: gamesModel
        }
        delegate: PlasmaExtras.ExpandableListItem {
            icon: "input-gamepad"
            title: executable
            subtitle: i18nc("@label", "PID: %1", String(processid))
        }

        QQC2.Action {
            id: helpAction
            icon.name: "help-contextual"
            text: i18nc("@action open help url", "Help!")
            onTriggered: Qt.openUrlExternally('https://github.com/FeralInteractive/gamemode')
        }

        PlasmaExtras.PlaceholderMessage {
            visible: gamesModel.serviceRegistered && gamesEmpty()
            anchors.centerIn: parent
            width: parent.width - (PlasmaCore.Units.largeSpacing * 4)

            text: i18nc("@info", "No Processes in Gamemode")
            helpfulAction: helpAction
        }

        PlasmaExtras.PlaceholderMessage {
            visible: !gamesModel.serviceRegistered
            anchors.centerIn: parent
            width: parent.width - (PlasmaCore.Units.largeSpacing * 4)

            text: i18nc("@info", "Gamemode Service not Running")
            helpfulAction: helpAction
        }
    }
}
