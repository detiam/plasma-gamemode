��    
      l      �       �      �           #  "   ;     ^  7   }     �     �  :   �  �    	   �     �     �     �     �               ,  0   E     
                                 	       @action open help urlHelp! @infoGamemode Active @infoGamemode Inactive @infoGamemode Service not Running @infoNo Processes in Gamemode @info process count in gamemode%1 Process %1 Processes @labelPID: %1 Processes in Gamemode Processes that have their resources adjusted by 'gamemode' Project-Id-Version: gamemode
Report-Msgid-Bugs-To: https://invent.kde.org/sitter/plasma-gamemode
PO-Revision-Date: 2023-09-15 03:21+0800
Last-Translator: detiam <dehe_tian@outlook.com>
Language-Team: Chinese <kde-i18n-doc@kde.org>
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: Lokalize 23.08.1
 帮助！ 游戏模式活跃 游戏模式不活跃 游戏模式服务未运行 游戏模式下无进程 %1 个进程 进程号: %1 游戏模式中的进程 通过“游戏模式”调整其资源的进程 